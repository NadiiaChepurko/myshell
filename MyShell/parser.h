#ifndef __MyShell__parser__
#define __MyShell__parser__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define MAXARGS 50
#define MAXLINE 512
#define READ_END 0
#define WRITE_END 1

struct cmd{
    char *command[2+MAXARGS]; /* stores pointers to tokens from command line string */
    struct cmd *next;
};

int parse(char *);           /* function that parse command line string on tokens */
char* advance(char *);        /* helper for parse with core logic for tokenization */

#endif
