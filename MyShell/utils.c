#include "utils.h"

struct cmd *cmdListFront;

void printLL(){
    struct cmd *ptr = cmdListFront;
    char **temp;

    for(; ptr != NULL; ptr = ptr->next){
        temp = ptr->command;
        while(*temp){
            printf("%s\n", *temp);
            temp++;
        }
        printf("\n");
    }
}

void freeLL(){
    struct cmd *ptr = cmdListFront;
    struct cmd *temp;
    while(ptr != NULL){
        temp = ptr;
        ptr = ptr->next;
        free(temp);
    }
}
