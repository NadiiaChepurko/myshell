#include "runner.h"

struct cmd *cmdListFront;  /* front node to linked list of commands */

void run(){
    int   filedes[2];
    int pid, status;
    int   fd_in = 0;
    struct cmd *ptr = cmdListFront;
    int lastfd = -1;	/* pk: previous read end of the pipe */
    
    while (ptr != NULL)
    {
        if(checkBuiltinAndRun(ptr->command)){
            ptr = ptr->next;
        }else{
            pipe(filedes);
            if ((pid = fork()) == -1){
                perror("fork");
                exit(EXIT_FAILURE);
            }else if (pid == 0){
                dup2(fd_in, 0);
                if (ptr->next != NULL)
                    dup2(filedes[1], 1);
                close(filedes[0]);
                execvp(ptr->command[0], ptr->command);
                exit(EXIT_FAILURE);
            }else{
                if (lastfd > 0)
                    close(lastfd);	/* close the read file descriptor for the previous pipe */
                close(filedes[1]);
                if (ptr->next != NULL) {	/* pk */
                    fd_in = filedes[0];
                    lastfd = fd_in;
                }
                ptr = ptr->next;
            }
        }
    }
    
    while ((pid = wait(&status)) != -1){
        fprintf(stderr, "process %d exits with %d\n", pid, WEXITSTATUS(status));
    }
}
