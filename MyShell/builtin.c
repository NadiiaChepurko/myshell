#include "builtin.h"

struct builtin *table[CMDNUM];

void freeBuiltinsTable(){
    int i = 0;
    for(i = 0; i < CMDNUM; i++){
        free(table[i]);
    }
}

void initializeBuiltins(){
    
    table[0] = calloc(1, sizeof(struct builtin));
    table[0]->name = "exit";
    table[0]->f = my_exit;
    
    table[1] = calloc(1, sizeof(struct builtin));
    table[1]->name = "cd";
    table[1]->f = my_cd;
    
}

int checkBuiltinAndRun(char **cmd){
    int i = 0;
    for(i = 0; i < CMDNUM; i++){
        if(strcmp(table[i]->name, *cmd) == 0){
            ((table[i]->f)(getargc(cmd), cmd));
            return 1;
        }
    }
    return 0;
}

int my_cd(int argc, char**argv){
    
    if(argc > 2){
        printf("cd: too many arguments.\n");
        return -1;
    }else if(argc == 1){
        if (chdir(getenv("HOME")) != 0){
            perror("chdir");
            return -1;
        }
    }else if (chdir(argv[1]) != 0){
       perror("chdir");
       return -1;
    }
    return 0;
}

int my_exit(int argc, char**argv){
    
    if(argc > 2){
        printf("Error: too many arguments, must be one or zero.\n");
        return -1;
    }
    
    if(argc == 1){
        exit(0);
    }
    
    char *ptr = argv[1];
    
    while(*ptr){
        if(!isdigit(*ptr)){
            printf("Error: non-integer argument.\n");
            return -1;
        }
        ptr++;
    }
    
    exit(atoi(argv[1]));
}

int getargc(char **cmd){
    int argc = 0;
    int i = 0;
    while(cmd[i++]) argc++;
    return argc;
}