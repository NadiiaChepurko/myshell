#ifndef __MyShell__utils__
#define __MyShell__utils__

#include <stdio.h>
#include "parser.h"

void printLL(); /* helper function to print linked list of commands and their args */
void freeLL();  /* deallocate space for linked list */

#endif 