#include "parser.h"
#include "utils.h"
#include "runner.h"
#include "builtin.h"

struct cmd *cmdListFront;

int main(int argc, const char * argv[]) {
    
    char *line = calloc(MAXLINE+1, 1);
    
    chdir("/Users/nadiachepurko/Desktop");
    
    initializeBuiltins();
    int give_prompt = isatty(0);    /* check that input comes from the Terminal */
    if (give_prompt)fputs(">: ", stderr);
    
    while (fgets(line, MAXLINE+1, stdin)) {
        if (parse(line) != -1){
            if(cmdListFront != NULL) run();
        }
        freeLL();
        if (give_prompt) fputs("\n>: ", stderr);
    }
    
    freeBuiltinsTable();
    free(line);
    return 0;
}


