#include "parser.h"

extern struct cmd *cmdListFront;
struct cmd *currentNode;
int isCommand;
int j; /* current index to fill in for an array of pointers in cmd struct */
int isPipe;

int parse(char *line){
    
    int isStart = 1;
    int argCount = 0; /* count number of arguments */
    isCommand = 1;
    j = 0;
    isPipe = 0;
    
    cmdListFront = NULL;
    while(*line && (*line == ' ' || *line == '\n' || *line == '\t'))line++;
    if(*line == EOF) exit(0);
    if(strlen(line) < 1) return 0;
    if(*line == '|'){
        printf("Error: command cannot be a pipe.\n");
        return -1;
    }
    char *temp = line;
    cmdListFront = calloc(1, sizeof(struct cmd));
    currentNode = cmdListFront;
    
     while(*temp && *temp != '\n' && *temp != EOF){
      
         while(*temp == ' ' || *temp == '\t') temp++;
         
         if(*temp == '|' || isPipe){
             isCommand = 1;
             argCount = 0;
             j = 0;
             while(*temp == '|') temp++; /* skip all consequtive pipes */
             isPipe = 0;
             continue;
         }
         
         if(isCommand){
             if(*temp != '\n' && *temp != '\0' && *temp != EOF){
                 if(!isStart){
                     currentNode->next = calloc(1, sizeof(struct cmd));
                     currentNode = currentNode->next;
                 }else{
                     isStart = 0;
                 }
                 temp = advance(temp);
                 isCommand = 0;
             }
         }else{
             if(*temp != '\n' && *temp != '\0' && *temp != EOF){
                 argCount++;
                 if(argCount > 50){
                     printf("Too many arguments (50 max).\n");
                     return -1;
                 }
                 temp = advance(temp);
             }else{
                 return 0;
             }
         }
     }
    return 0;
}

char* advance(char *temp){
    if(*temp == '\"'){
        temp++;
        if(*temp != '\"' && *temp != '\n' && *temp != EOF){
            currentNode->command[j] = temp;
        }
        for(; *temp != '\"' && *temp != '\n' && *temp != EOF; temp++);
        if(*temp != '\"'){
            printf("Warning: mismatched quote, unexpected result may occur.\n");
        }
        *temp++ = 0;
        j++;
    }else if(*temp == '\''){
        temp++;
        if(*temp != '\'' && *temp != '\n' && *temp != EOF){
            currentNode->command[j] = temp;
        }
        for(; *temp != '\'' && *temp != '\n' && *temp != EOF; temp++);
        if(*temp != '\''){
            printf("Warning: mismatched quote, unexpected result may occur.\n");
        }
        *temp++ = 0;
        j++;
    }else{
        if(*temp != ' ' && *temp != '\t' && *temp != '|' && *temp != '\n' && *temp != EOF){
             currentNode->command[j] = temp;
        }
        for(; *temp != ' ' && *temp != '\t' && *temp != '|' && *temp != '\n' && *temp != EOF; temp++);
        if(*temp == '|') isPipe = 1;
        *temp++ = 0;
        j++;
    }
    return temp;
}