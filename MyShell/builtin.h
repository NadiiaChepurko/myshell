#ifndef __MyShell__builtin__
#define __MyShell__builtin__

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h> /* for chdir() */
#include <ctype.h> /* for isdigit() s*/
#include <string.h>

#define CMDNUM 2   /* number of builtin functions */

struct builtin{
    char *name;
    int (*f)(int, char**);
};

int my_cd(int argc, char**argv);    /* cd function */
int my_exit(int argc, char**argv);  /* exit function */
int checkBuiltinAndRun(char **);    /* check if function is a builtin and run if so */
void initializeBuiltins();          /* allocate space for builtins structs */
int getargc(char **);               /* get argc for a command */
void freeBuiltinsTable();           /* deallocate space for builtins structs */

#endif 